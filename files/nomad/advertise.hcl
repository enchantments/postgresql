advertise {
  http = "{{ GetPrivateInterfaces | include \"rfc\" \"1918\" | attr \"address\" }}"
  rpc  = "{{ GetPrivateInterfaces | include \"rfc\" \"1918\" | attr \"address\" }}"
  serf = "{{ GetPrivateInterfaces | include \"rfc\" \"1918\" | attr \"address\" }}"
}
