consul {
  address   = "127.0.0.1:8501"
  ssl       = true
  ca_file   = "/etc/tls/ca.pem"
  key_file  = "/etc/tls/key.pem"
  cert_file = "/etc/tls/cert.pem"
}
