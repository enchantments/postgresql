client {
  enabled = true

  host_volume "postgresql-database" {
    path = "/var/lib/postgresql"
    read_only = false
  }
}
