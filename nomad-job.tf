resource "nomad_job" "postgresql" {
  jobspec = templatefile("${path.module}/nomad-job.hcl", {
    postgres_password = random_string.random.result
   })
}
