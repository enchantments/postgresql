job "postgresql" {
  datacenters = ["gce-us-west1-a"]
  type        = "service"

  group "postgresql" {
    count = 1

    volume "postgresql" {
      type      = "host"
      source    = "postgresql-database"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "postgresql" {
      driver = "docker"

    //    volume_mount {
    //      volume      = "postgresql-database"
    //      destination = "/var/lib/postgresql/data"
    //    }

      env = {
        "POSTGRES_PASSWORD" = "sadflkewjlfsdkjcxa"
      }

      config {
        image = "postgres:12"

        port_map {
          db = 5432
        }
      }

      resources {
        cpu    = 2000
        memory = 3000

        network {
          port "db" {
            static = 5432
          }
        }
      }

      service {
        name = "postgresql"
        port = "db"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
