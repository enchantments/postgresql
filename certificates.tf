resource "tls_private_key" "instance" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "instance" {
  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.instance.private_key_pem

  subject {
    common_name = "Compute Pool Instance"
  }

  dns_names = [
    "server.${var.datacenter}.consul",
    "client.${var.region}.nomad",
    "localhost",
  ]

  ip_addresses = [
    "127.0.0.1"
  ]
}

resource "vault_pki_secret_backend_sign" "compute" {
  backend = var.pki_backend_path
  name    = var.pki_role_name

  csr         = tls_cert_request.instance.cert_request_pem
  common_name = tls_cert_request.instance.subject[0].common_name
}
