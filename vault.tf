provider "vault" {
  address      = var.pki_vault_address
  token        = var.pki_vault_token
  ca_cert_file = var.pki_ca_cert_file
}
