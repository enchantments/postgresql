resource "google_compute_disk" "database" {
  name    = "postgresql-database"
  type    = "pd-ssd"
  project = var.project
  zone    = var.zone
  size    = var.size
}

resource "google_compute_attached_disk" "default" {
  disk        = google_compute_disk.database.self_link
  instance    = google_compute_instance.database.self_link
  device_name = "postgresql-database"
  project     = var.project
  zone        = var.zone
}
