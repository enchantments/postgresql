#!/usr/bin/env bash

set -e

BLOCK_DEVICE=/dev/disk/by-id/google-postgresql-database
MOUNT_POINT=/var/lib/postgresql

sudo mkfs.ext4 ${BLOCK_DEVICE} || true
sudo mkdir --parents ${MOUNT_POINT}
sudo mount ${BLOCK_DEVICE} ${MOUNT_POINT}

sync
